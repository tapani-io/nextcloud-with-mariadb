# Install Nextcloud manually with MariaDB in Raspbian

Here is a tutorial how to install Nextcloud with MariaDB in Raspbian (Raspberry Pi). It took me few tries to get it working properly so I thought to write a tutorial for myself.

## Install required packages

    sudo apt install apache2 mariadb-server libapache2-mod-php7.3
    sudo apt install php7.3-gd php7.3-json php7.3-mysql php7.3-curl php7.3-mbstring
    sudo apt install php7.3-intl php-imagick php7.3-xml php7.3-zip

## Set up MariaDB

First, we do the initial set up. Just follow the instructions.

    sudo mysql_secure_installation

Then, we create the database for Nextcloud:

    sudo mysql -u root
    
    create database nextcloud;
    create user nextclouduser@localhost identified by 'your-password';
    grant all privileges on nextcloud.* to nextclouduser@localhost identified by 'your-password';
    flush privileges;
    exit;

## Download Nextcloud files

Download the nextcloud files. Replace 17.0.0 with your latest version.

    wget https://download.nextcloud.com/server/releases/nextcloud-17.0.0.tar.bz2
    wget https://download.nextcloud.com/server/releases/nextcloud-17.0.0.tar.bz2.asc
    wget https://download.nextcloud.com/server/releases/nextcloud-17.0.0.tar.bz2.sha256
    wget https://download.nextcloud.com/server/releases/nextcloud-17.0.0.tar.bz2.sha512

## Verify integrity

When downloading and installing packages, it's important to verify the packages. This ensures the files have not been tampered with by malicious people.

Verify the checksums:

    sha256sum -c nextcloud-17.0.0.tar.bz2.sha256 < nextcloud-17.0.0.tar.bz2
    sha512sum -c nextcloud-17.0.0.tar.bz2.sha512 < nextcloud-17.0.0.tar-bz2

Verify the PGP signature:

    wget https://nextcloud.com/nextcloud.asc
    gpg --import nextcloud.asc
    gpg --verify nextcloud-17.0.0.tar.bz2.asc nextcloud-17.0.0.tar.bz2

## Unzip Nextcloud installation file

    cd /var/www/html
    tar -xjfv nextcloud-17.0.0.tar.bz2

## Change Nextcloud directory permission

You must permit www-data access to the nextcloud directory.

    sudo chown -R www-data:www-data /var/www/html/nextcloud

## Set up external data directory

To create a directory where the data is stored, do this:

    mkdir -p /path/to/data
    sudo chown -R www-data:www-data /path/to/data
    sudo chmod 750 /path/to/data

## Install

1. Browse to your Raspberry Pi's IP (e.g. 192.168.1.1/nextcloud)
2. Create an user
3. Set data directory path
4. Use the same user you created earlier for MariaDB
5. Use the same password you created earlier for MariaDB
6. Install

## Change memory limit

By default, the memory limit is 128mb which is too low. We should use 512mb.

    sudo vim /etc/php/7.3/apache2/config/php.ini

## Install cache

We should use caching for better performance.

    sudo apt install php-apcu

Add the following to /var/www/html/nextcloud/config/config.php

    'memcache.local' => '\OC\Memcache\APCu',